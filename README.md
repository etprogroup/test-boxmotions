# BOXMOTIONS : Technical Test

## Install Environment (Previously)

If you have installed `composer`, `synfony`. You should install the project now.

### Install Composer
- [Download and install composer](https://getcomposer.org/Composer-Setup.exe)


## Install project

### Download project

- [Download from git](git clone https://etpro@bitbucket.org/etprogroup/test-boxmotions.git)

> `git clone https://etpro@bitbucket.org/etprogroup/test-boxmotions.git` 

Then clone the repository in with you sourcetree or other program for git repositories. if not use programs, you can use command line as: 

- cd [path/project]
- git remote add origin `[url/repository]`. *Example:* `git remote add origin https://etpro@bitbucket.org/etprogroup/test-boxmotions.git`
- git pull origin master

`Important:` You should install git previously, if you don't have it. [Install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- In command line from project path, write:

> `cd [path/project]` and then `composer install`

if have any version problem or requirement, write: `composer install --ignore-platform-reqs`

### Config Database

Configure the file `boxmotions.sql` in you local database:
 
Configure `.env` with your local configuration

## More 

In the project, I use framework Synfony4.4 and libraries cdn like jQuery, DataTable.

````
Práctica php

El Edificio Wayne necesita una aplicación para gestionar el uso de los ascensores
asegurando su uso óptimo.

Datos
El edificio tiene 4 plantas (3 + planta baja) y 3 ascensores.
Estas son las secuencias de funcionamiento de los ascensores (peticiones):
1. Cada 5 minutos de 09:00h a 11:00h llaman al ascensor desde la planta baja para ir a las
planta 2
2. Cada 5 minutos de 09:00h a 11:00h llaman al ascensor desde la planta baja para ir a las
planta 3
3. Cada 10 minutos de 09:00h a 10:00h llaman al ascensor desde la planta baja para a las
planta 1
4. Cada 20 minutos de 11:00h a 18:20h llaman al ascensor desde la planta baja para ir a
todas las plantas
5. Cada 4 minutos de 14:00h a 15:00h llaman al ascensor desde las plantas 1, 2 y 3 para ir
a la planta baja
6. Cada 7 minutos de 15:00h a 16:00h llaman al ascensor desde las plantas 2 y 3 para ir a
la planta baja
7. Cada 7 minutos de 15:00h a 16:00h llaman al ascensor desde la planta baja para ir a las
plantas 1 y 3
8. Cada 3 minutos de 18:00h a 20:00h llaman al ascensor desde las plantas 1, 2 y 3 para ir
a la planta baja

Resultado
La aplicación debe obtener como resultado un informe de la posición de todos los
ascensores en cada petición, el número de plantas recorridas en la petición, y el total de
plantas recorridas por ascensor.
A efectos de cómputo, los ascensores se mueven de forma instantánea desde cualquier
piso a otro, pero se suman todas las plantas recorridas y solo se puede realizar un
movimiento completo (origen → llamada, llamada → destino) por petición en el mismo
Instante.

Valoraciones y consideraciones
● La estrategia de asignación de ascensores en cada petición
● Se valorará el uso de un framework MVC
● Se valorará el uso de BBDD
● Escalabilidad (Por ejemplo, que la aplicación pueda trabajar con 4 ascensores)
● Esté finalizada
● No contenga errores
● El código esté documentado y sea fácil de leer
● Tenga interfaz maquetada (JS, CSS, ..)
````