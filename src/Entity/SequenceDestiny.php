<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceDestiny
 *
 * @ORM\Table(name="sequence_destiny")
 * @ORM\Entity
 */
class SequenceDestiny
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int
     *
     * @ORM\Column(name="floor_id", type="integer", nullable=false)
     */
    public $floorId;

    /**
     * @var int
     *
     * @ORM\Column(name="sequence_id", type="integer", nullable=false)
     */
    public $sequenceId;


    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Sequence", inversedBy="SequenceDestiny")
     * @ORM\JoinColumn(name="sequence_id", referencedColumnName="id")
     */
    public $sequence;


}
