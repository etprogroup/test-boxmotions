<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sequence
 *
 * @ORM\Table(name="sequence")
 * @ORM\Entity
 */
class Sequence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    public $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="minutes", type="integer", nullable=true)
     */
    public $minutes;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start", type="time", nullable=true)
     */
    public $start;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end", type="time", nullable=true)
     */
    public $end;

    /**
     * @ORM\OneToMany(targetEntity="SequenceDestiny", mappedBy="sequence")
     */
    private $listDestiny;

    /**
     * @ORM\OneToMany(targetEntity="SequenceOrigin", mappedBy="sequence")
     */
    private $listOrigin;

    public function __construct()
    {
        $this->listDestiny = new ArrayCollection();
        $this->listOrigin = new ArrayCollection();
    }

    /**
     * @return Collection|SequenceOrigin[]
     */
    public function getListOrigin(): Collection
    {
        return $this->listOrigin;
    }

    public function setListOrigin(SequenceOrigin $listOrigin): self
    {
        $this->listOrigin = $listOrigin;
        return $this;
    }

    /**
     * @return Collection|SequenceDestiny[]
     */
    public function getListDestiny(): Collection
    {
        return $this->listDestiny;
    }

    public function setListDestiny(SequenceDestiny $listDestiny): self
    {
        $this->listDestiny = $listDestiny;
        return $this;
    }
}
