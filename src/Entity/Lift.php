<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lift
 *
 * @ORM\Table(name="lift")
 * @ORM\Entity
 */
class Lift
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    public $name;

    /**
     * @var int
     */
    public $position = 1;

    /**
     * @var array
     */
    public $positions = [];

    /**
     * Añado la posicion en la que se encuentra el ascensor y pro dnde ha pasado.
     * @param $position
     */
    public function addPosition($position){
        $this->position = $position;
        $this->positions[] = $position;
    }

    /**
     * @var array
     */
    public $sequences = [];

    /**
     * Obtengo informacion de la secuencia. que luego sera usada para ser mostrado en la tabla
     * @param array $data
     */
    public function addSequence($data = []){
        $this->sequences[] = [
            'hour' => isset($data['hour']) ? $data['hour'] : '',
            'position' => isset($data['position']) ? $data['position'] : '',
            'sequence' => isset($data['sequence']) ? $data['sequence'] : '',
            'origin' => isset($data['origin']) ? $data['origin'] : '',
            'destiny' => isset($data['destiny']) ? $data['destiny'] : '',
            'destinies' => isset($data['destinies']) ? $data['destinies'] : [],
            'total' => isset($data['total']) ? $data['total'] : '',
        ];
    }

    /**
     * @var int
     */
    public $used = 0;

    /**
     * Esta funcion me permite evaluar cuando fue la ultima vez que fue usado el ascensor de manera de distribuir su uso equitativamente.
     */
    public function setUsed(){
        $this->used = microtime(true);
    }

    /**
     * @var array
     */
    public $travel = [];

    /**
     * @var int
     */
    public $travelTotal = 0;

    /**
     * Obtengo el desplazamiento desde el sitio inicial del ascensor hasta a ultimo destino
     *
     * @param $origin
     * @param $liftInit
     * @param $destinies
     * @return mixed
     */
    public function setTravel($origin, $liftInit, $destinies){

        //obtengo el desplazamiento desde el sitio inicial del ascensor hasta el lugar de la peticion
        $betweenFloor = $liftInit - $origin;
        $travel = ($betweenFloor < 0 ? $betweenFloor*(-1) : $betweenFloor);
        $newFloor = $origin;

        //obtengo el desplazamiento desde el sitio de la peticion a los destino. y entre los destinos
        foreach($destinies as $destiny){
            $betweenFloor = $newFloor - $destiny;
            $travel = $travel+($betweenFloor < 0 ? $betweenFloor*(-1) : $betweenFloor);
            $newFloor = $destiny;
        }

        $this->travel[] = $travel;

        //añadir el recorrido al recorrido total del ascensor.
        $this->travelTotal = $this->travelTotal+$travel;
        return $travel;
    }
}
