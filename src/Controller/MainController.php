<?php

namespace App\Controller;

use App\Entity\Sequence;
use App\Entity\Lift;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(){

        //Obtengo los asensores
        $lifts = $this->getDoctrine()
            ->getRepository(Lift::class)
            ->findAll();

        $liftList = [];
        foreach ($lifts as $lift){
            $liftList[$lift->id] = $lift;
        }

        //Obtengo las secuencias
        $sequences = $this->getDoctrine()
            ->getRepository(Sequence::class)
            ->findAll();

        $hours = [];
        foreach($sequences as $sequence){
            $minutes = $sequence->minutes;
            $start = $sequence->start->getTimestamp();
            $end = $sequence->end->getTimestamp();
            $origins = $sequence->getListOrigin();
            $destinies = $sequence->getListDestiny();

            //creo las peticiones que se han realizado por rango de horas
            for($time = $start; $time<=$end; $time+=($minutes*60)){
                $hour = date('H:i:s', $time);
                if(!isset($hours[$hour])){
                    $hours[$hour] = [];
                }

                foreach($origins as $origin){
                    if(!isset($hours[$hour][$origin->floorId])){
                        $hours[$hour][$origin->floorId] = [];
                    }

                    foreach($destinies as $destiny){
                        if(!in_array($destiny->floorId, $hours[$hour][$origin->floorId])){
                            $hours[$hour][$origin->floorId][] = $destiny->floorId;
                        }
                    }
                }
            }
        }

        //ordernar horarios
        ksort($hours);

        foreach($hours as $hour => $origins){

            foreach($origins as $origin => $destinies){

                //ordernar destinos ASC
                sort($destinies);

                //evaluar el mejor ascensor que se ajusta a la posicion.
                $lift = $this->evalLift($liftList, $origin);

                if($lift){

                    //obtener la posicion inicial del ascensor antes de ser llamado
                    $liftOrigin = $lift->position;

                    $lift->setUsed();
                    $lift->addPosition($origin);
                    foreach($destinies as $destiny){
                        $lift->addPosition($destiny);
                    }


                    //obtener la posicion final del ascensor luego del ultimo destino
                    $liftDestiny = $lift->position;

                    //obtener el recorrido total del viaje en el asensor
                    $liftTravel = $lift->setTravel($origin, $liftOrigin, $destinies);

                    $lift->addSequence([
                        'hour' => $hour,
                        'position' => $origin,
                        'sequence' => null,
                        'origin' => $liftOrigin,
                        'destiny' => $liftDestiny,
                        'destinies' => $destinies,
                        'total' => $liftTravel,
                    ]);

                    $liftList[$lift->id] = $lift;
                }
            }

        }

        return $this->render('main/index.html.twig', [
            'liftList' => $liftList,
        ]);
    }


    /**
     * evaluar el mejor ascensor que se ajusta a la posicion.
     * En este caso tomo el ascensor mas cercano que no haya sido usado anteriormente.
     * Asi distribuir la carga de ascensores mas optima y uniforme.
     *
     * @param $lifts
     * @param $position
     * @return null
     */
    public function evalLift($lifts = [], $position = 1){
        $result = null;
        $results = [];
        foreach($lifts as $lift){

            //set default lift
            $result = $result ? $result : $lift;

            //get positions
            $positionResult = $position-$result->position;
            $positionLift = $position-$lift->position;

            //set new lift to closets position
            if($positionResult*$positionResult > $positionLift*$positionLift or ($positionResult*$positionResult == $positionLift*$positionLift &&  $result->used >= $lift->used)){
                $results[] = $lift;
                $result = $lift;
            }
        }

        return $result;
    }
}
